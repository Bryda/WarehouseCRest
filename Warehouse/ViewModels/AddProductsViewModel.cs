﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Model.Models;

namespace Warehouse.ViewModels
{
    public class AddProductsViewModel
    {  
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        public int Id { get; set; }

        [Required(ErrorMessage = "Product name is required!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Quantity field is required!")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Price field is required!")]
        [RegularExpression(@"((\d)*[,]\d\d)", ErrorMessage = "Price format is 123,45.")]
        public double Price { get; set; }

        [Display(Name = "Customer ID")]
        [Required(ErrorMessage = "Customer is required!")]
        public int CustomerId { get; set; }
        
        public virtual ICollection<CustomerProduct> CustomerProducts { get; set; }
    }
}