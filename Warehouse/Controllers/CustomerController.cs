﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using System.Web.Mvc;
using NLog;
using Warehouse.Data.DataBase;
using Warehouse.Model.Models;
using Warehouse.Service;
using Warehouse.Service.InterfacesServices;
using Warehouse.Service.Services;
using Warehouse.ViewModels;

namespace Warehouse.Controllers
{
    //[System.Web.Mvc.Authorize]
    public class CustomerController : ApiController
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private ICustomerService _customerService;
        private IProductService _productService;

        public CustomerController(ICustomerService customerService, IProductService productService)
        {
            _customerService = customerService;
            _productService = productService;
        }

        // GET: Customer
        public IEnumerable<Customer> GetCustomers()
        {
            return _customerService.GetAll();
        }

        // GET:Customer/1
        [ResponseType(typeof(Customer))]
        public IHttpActionResult GetCustomer(int id)
        {
            Customer customer = _customerService.FindById(id);
            if (customer == null)
            {
                return NotFound();
            }
            customer.CustomerProducts = _customerService.FindAllCustomerProducts(id).ToList();
            return Ok(customer);

        }
        
        // POST: Customer
        [ResponseType(typeof(Customer))]
        public IHttpActionResult PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _customerService.Create(customer);
            _logger.Info("Created new customer:{0} {1}", customer.FirstName, customer.LastName);

            return CreatedAtRoute("DefaultApi", new { id = customer.Id }, customer);
        }
        
        // PUT: Customer/1
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCustomer(int id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Customer customerFound = _customerService.FindById(id);
                customerFound.LastName = customer.LastName;
                customerFound.FirstName = customer.FirstName;

                _customerService.Edit(customerFound);
                _logger.Info("Customer {0} {1} was edited", customer.FirstName, customer.LastName);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_customerService.FindById(id) == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        
        //DELETE: Customer/1
        [ResponseType(typeof(Customer))]
        public IHttpActionResult DeleteCustomer(int id)
        {
            Customer customer = _customerService.FindById(id);
            if (customer == null)
            {
                return NotFound();
            }

            _customerService.Delete(customer);
            _logger.Info("Deleted customer: {0} {1}", customer.FirstName, customer.LastName);

            return Ok(customer);
        }
    }
}
