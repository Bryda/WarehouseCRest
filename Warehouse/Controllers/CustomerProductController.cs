﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using NLog;
using Warehouse.Data.DataBase;
using Warehouse.Model.Models;
using Warehouse.Service.InterfacesServices;

namespace Warehouse.Controllers
{
    public class CustomerProductController : ApiController
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private ICustomerService _customerService;
        private IProductService _productService;

        public CustomerProductController(ICustomerService customerService, IProductService productService)
        {
            _customerService = customerService;
            _productService = productService;
        }

        // PUT: CustomerProduct/1
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCustomerProduct(int id, Product product)
        {
            try
            {
                    var prod = _productService.Find(productTmp => productTmp.Name.Equals(product.Name));
                    if (prod != null)
                    {
                        if (prod.Quantity >= product.Quantity)
                        {
                            CustomerProduct customerProduct = new CustomerProduct()
                            {
                                CustomerId = id,
                                Name = product.Name,
                                Price = product.Price,
                                Quantity = product.Quantity
                            };
                            _customerService.SaveCustomerProduct(customerProduct);
                            prod.Quantity -= product.Quantity;
                            _productService.Edit(prod);
                        }
                    }
 
                _logger.Info("Product {0} add to customer {1}", product.Name, id);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_customerService.FindById(id) == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: CustomerProduct/1
        [ResponseType(typeof(void))]
        public IHttpActionResult PostEditCustomerProduct(int id, Product product)
        {
            var customerProduct = _customerService.FindCustomerProduct(id);

            try
            {
                customerProduct.Name = product.Name;
                customerProduct.Price = product.Price;
                customerProduct.Quantity = product.Quantity;

                _customerService.EditCustomerProduct(customerProduct);
                _logger.Info("Product {0} for customer {1} was edited", customerProduct.Name, customerProduct.Customer.Id);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_customerService.FindCustomerProduct(id) == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //DELETE: CustomerProduct/1
        [ResponseType(typeof(Customer))]
        public IHttpActionResult DeleteCustomerProduct(int id)
        {
            var product = _customerService.FindCustomerProduct(id);
            if (product == null)
            {
                return NotFound();
            }
            _customerService.DeleteCustomerProduct(product);
            _logger.Info("Product {0} {1} {2} was deleted", _customerService.FindById(product.CustomerId).FirstName, _customerService.FindById(product.CustomerId).LastName, product.Name);

            return Ok(product);
        }

    }
}
