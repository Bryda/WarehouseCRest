﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using NLog;
using Warehouse.Data.DataBase;
using Warehouse.Model.Models;
using Warehouse.Service.InterfacesServices;

namespace Warehouse.Controllers
{
    public class CategoryController : ApiController
    {
        
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // GET: Category
        public IEnumerable<Category> GetCategories()
        {
            return _categoryService.GetAll();
        }

        // GET: Category/1
        [ResponseType(typeof(Category))]
        public IHttpActionResult GetCategory(int id)
        {
            Category category = _categoryService.FindById(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }

        // PUT: Category/1
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCategory(int id, Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.Id)
            {
                return BadRequest();
            }

            try
            {
                _categoryService.Edit(category);
                logger.Info("Category {0} was edited", category.Name);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_categoryService.FindById(id) == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: Category
        [ResponseType(typeof(Category))]
        public IHttpActionResult PostCategory(Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _categoryService.Create(category);
            logger.Info("Created new category:{0}", category.Name);

            return CreatedAtRoute("DefaultApi", new { id = category.Id }, category);
        }

        // DELETE: Category/1
        [ResponseType(typeof(Category))]
        public IHttpActionResult DeleteCategory(int id)
        {
            Category category = _categoryService.FindById(id);
            if (category == null)
            {
                return NotFound();
            }

            _categoryService.Delete(category);
            logger.Info("Deleted category: {0}", category.Name);

            return Ok(category);
        }

    }
}
