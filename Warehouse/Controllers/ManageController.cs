﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NLog;
using Warehouse.Data.DataBase;
using Warehouse.Data.Enums;
using Warehouse.Model.Models;
using Warehouse.ViewModels;

namespace Warehouse.Controllers
{
    [System.Web.Mvc.Authorize]
    public class ManageController : ApiController
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private ApplicationDbContext context;

        public ManageController()
        {
            context = new ApplicationDbContext();
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion

        // GET: Manage
        public IEnumerable<UserViewModel> GetUsers()
        {
            var roles = System.Enum.GetNames(typeof(RolesEnum)).ToList();
            List<UserViewModel> userVM = new List<UserViewModel>();
            roles.ForEach((role) =>
            {
                var roleDb = (from r in context.Roles where r.Name.Contains(role) select r).FirstOrDefault();
                var users = context.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(roleDb.Id)).ToList();

                var userList = users.Select(user => new UserViewModel
                {
                    UserId = user.Id,
                    Username = user.UserName,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    RoleName = role
                }).ToList();
                userVM.AddRange(userList);
            });

            return userVM;
        }

        // PUT: manage/1
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(string id, ApplicationUser model)
        {

            if (ModelState.IsValid)
            {
                var store = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(store);
                ApplicationUser user = userManager.FindByNameAsync(model.UserName).Result;
                var roleDb = (from r in context.Roles where r.Id.Contains(user.Id) select r).FirstOrDefault();
                if (user != null)
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Email = model.Email;
                    user.PhoneNumber = model.PhoneNumber;

                    if (User.IsInRole("Admin"))
                        
                    if (model.RoleName.IsNullOrWhiteSpace()/*Request.Params.Get("UserRole").IsNullOrWhiteSpace()*/)
                        {
                            return BadRequest(ModelState);
                        }
                        else
                        {//TODO:fix change  user role
                            ChangeUserRole(user.Id, model.RoleName);
                        }

                    context.Entry(user).State = EntityState.Modified;
                    context.SaveChanges();
                    _logger.Info("Edited user {0} {1}", user.FirstName, user.LastName);
                }

                return StatusCode(HttpStatusCode.NoContent);

            }

            return BadRequest(ModelState);

        }

        private void ChangeUserRole(string userId, string userRole)
        {
            var UserManger = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var currentRoles = System.Enum.GetNames(typeof(RolesEnum)).ToList();
            currentRoles.ForEach(role => UserManger.RemoveFromRole(userId, userRole));
            UserManger.AddToRole(userId, userRole);
        }

        // DELETE: Manage/1
        [ResponseType(typeof(Category))]
        public IHttpActionResult Delete(string id)
        {
            var user = UserManager.Users.SingleOrDefault(u => u.Id == id.ToString());
            if (user == null)
            {
                return NotFound();
            }
            UserManager.Delete(user);
            _logger.Info("Deleted user {0} {1}", user.FirstName, user.LastName);            

            return Ok();
        }

        // PUT: manage/{model}
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = UserManager.FindByEmail(model.Email);
            if (user == null)
            {
                return NotFound();
            }
            var result = UserManager.RemovePassword(user.Id);
            result = UserManager.AddPassword(user.Id, model.Password);
            if (result.Succeeded)
            {
                _logger.Info("Reset user password for {0} {1}", user.FirstName, user.LastName);
                return StatusCode(HttpStatusCode.NoContent);
            }
            return Conflict();

        }
    }
}