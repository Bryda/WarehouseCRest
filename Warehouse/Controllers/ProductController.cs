﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using NLog;
using Warehouse.Model.Models;
using Warehouse.Service;
using Warehouse.Service.InterfacesServices;
using Warehouse.ViewModels;
using System.Web.Http;
using System.Web;
using Warehouse.Data.Repositories;
using Warehouse.Service.Services;

namespace Warehouse.Controllers
{
    //[System.Web.Http.Authorize]
    public class ProductController : ApiController
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private IProductService _productService;
        private ICategoryService _categoryService;

        public ProductController(IProductService productService, ICategoryService categoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }

        // GET: Product
        public IEnumerable<Product> Get()
        {            
            return _productService.GetAll().ToList(); 
        }


        // GET: Product/1
        public HttpResponseMessage Get(int id)
        {            
            var product = _productService.FindById(id);
            if (product != null)
            {
                HttpResponseMessage response = Request.CreateResponse(product);                
                return response;                
            }                
            else
            {
                var response = new HttpResponseMessage(HttpStatusCode.NotFound);
                response.Content = new StringContent("No Product with id: " + id + ", was found.");
                throw new HttpResponseException(response);
            }
        }
        
        // PUT: Product/1
        public HttpResponseMessage Put(int id, EditProductViewModel product)
        {
            try
            {
                var saveProduct = new Product()
                {
                    Id = id,
                    Name = product.Name,
                    Price = product.Price,
                    Quantity = product.Quantity,
                    CategoryId = _categoryService.FindCategoryByName(product.CategoryName).Id
                };
                _productService.Edit(saveProduct);
               
                // Acceptable status codes are 200/201/204
                var response = new HttpResponseMessage(HttpStatusCode.Accepted);
                response.Headers.Location = new Uri(Request.RequestUri.ToString());
                return response;
            }
            catch (Exception e)
            {
                var response = new HttpResponseMessage(HttpStatusCode.Conflict);
                response.Content = new StringContent(e.Message);
                throw new HttpResponseException(response);
            }
        }
        
        // DELETE: api/Product/5
        public HttpResponseMessage Delete(int id)
        {
            var product = _productService.FindById(id);
            _productService.Delete(product);
            _logger.Info("Deleted product {0}", product.Name);
            
            // Acceptable status codes are 200/202/204
            var response = new HttpResponseMessage(HttpStatusCode.Accepted);
            return response;            
        }

        // POST: api/Product
        public HttpResponseMessage Post(Product product)
        {
            try
            {                
                _productService.Create(product);            

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);                
                response.Headers.Location = new Uri(VirtualPathUtility.AppendTrailingSlash(Request.RequestUri.ToString()) + product.Id);
                return response;                
            }
            catch (Exception e)
            {
                var response = new HttpResponseMessage(HttpStatusCode.Conflict);
                response.Content = new StringContent(e.Message);
                throw new HttpResponseException(response);
            }
        }
        
    }
}