﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Newtonsoft.Json;
using Warehouse.App_Start;
using Warehouse.Data.DataBase;

namespace Warehouse
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);                 
            WebApiConfig.Register();
            AutofacConfig.ConfigureContainer();
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Re‌​ferenceLoopHandling = ReferenceLoopHandling.Ignore;
            InitializeDatabase();
        }

        private void InitializeDatabase()
        {
            Database.SetInitializer(new InitializeDb());
            using (var db = new ApplicationDbContext())
            {
                {
                    db.Database.Initialize(true);
                }
            }
        }
    }
}
