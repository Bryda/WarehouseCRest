﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.WebApi;
using Warehouse.Controllers;
using Warehouse.Data.Infrastructure;
using Warehouse.Data.Infstracture;
using Warehouse.Data.Repositories;
using Warehouse.Model.Models;
using Warehouse.Service;
using Warehouse.Service.InterfacesServices;
using Warehouse.Service.Services;

namespace Warehouse.App_Start
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<CategoryService>().As<ICategoryService>();
            builder.RegisterType<CustomerService>().As<ICustomerService>();
            builder.RegisterType<ProductService>().As<IProductService>();

            builder.RegisterType<ProductRepository>().As<IProductRepository>();
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);


            //var builder = new ContainerBuilder();

            ////builder.RegisterControllers(typeof(MvcApplication).Assembly);
            ////builder.regRegisterControllers(typeof(ProductController).Assembly);

            //builder.RegisterType<CategoryService>().As<ICategoryService>();
            //builder.RegisterType<CustomerService>().As<ICustomerService>();
            //builder.RegisterType<ProductService>().As<IProductService>();

            //builder.RegisterType<ProductRepository>().As<IProductRepository>();
            //builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
            //builder.RegisterType<CategoryRepository>().As<ICategoryRepository>();

            //var container = builder.Build();

            //DependencyResolver.SetResolver(new AutofacWebApiDependencyResolver(container));
        }
    }
}