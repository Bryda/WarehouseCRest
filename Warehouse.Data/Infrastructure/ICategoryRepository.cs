﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Data.Infstracture;
using Warehouse.Model.Models;

namespace Warehouse.Data.Infrastructure
{
    public interface ICategoryRepository: IRepository<Category>
    {
        Category findByName(string name);
    }
}
