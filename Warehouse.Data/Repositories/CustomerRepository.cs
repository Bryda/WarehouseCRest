﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Warehouse.Data.Infrastructure;
using Warehouse.Model.Models;

namespace Warehouse.Data.Repositories
{
    public class CustomerRepository:Repository<Customer>, ICustomerRepository
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public IEnumerable<CustomerProduct> GetCustomerProducts(int customerId)
        {
            return Context.Set<CustomerProduct>().Where(product => product.CustomerId.Equals(customerId));            
        }

        public void SaveCustomerProduct(CustomerProduct customerProduct)
        {
            try
            {
                Context.Set<CustomerProduct>().Add(customerProduct);
                Context.SaveChanges();
            }
            catch (Exception)
            {
                _logger.Error("Can not save product {2} to customer {1} {0}", customerProduct.Customer.FirstName,customerProduct.Customer.LastName,customerProduct.Name);
                throw;
            }
            
            
        }

        public CustomerProduct GetCustomerProduct(int id)
        {
            return Context.Set<CustomerProduct>().FirstOrDefault(product => product.Id.Equals(id));
        }

        public void EditCustomerProduct(CustomerProduct customerProduct)
        {
            try
            {
                Context.Set<CustomerProduct>().Attach(customerProduct);
                Context.Entry(customerProduct).State = EntityState.Modified;
                Context.SaveChanges();
            }
            catch (Exception)
            {
                _logger.Warn("Can not edit product {2} for customer {1} {0}", customerProduct.Customer.FirstName, customerProduct.Customer.LastName, customerProduct.Name);
                throw;
            }
           
        }

        public void DeleteCustomerProduct(CustomerProduct customerProduct)
        {
            try
            {
                Context.Set<CustomerProduct>().Remove(customerProduct);
                Context.SaveChanges();
            }
            catch (Exception)
            {
                _logger.Warn("Can not delete product {2} from customer {1} {0}", customerProduct.Customer.FirstName, customerProduct.Customer.LastName, customerProduct.Name);
                throw;
            }
            
        }
    }
}
