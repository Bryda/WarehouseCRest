﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Model.Models;

namespace Warehouse.Service.InterfacesServices
{
    public interface ICustomerService:IService<Customer>
    {
        IEnumerable<CustomerProduct> FindAllCustomerProducts(int customerId);

        void SaveCustomerProduct(CustomerProduct customerProduct);

        CustomerProduct FindCustomerProduct(int id);

        void EditCustomerProduct(CustomerProduct customerProduct);

        void DeleteCustomerProduct(CustomerProduct customerProduct);
    }
}
