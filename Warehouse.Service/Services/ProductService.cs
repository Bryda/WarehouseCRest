﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Data.Infstracture;
using Warehouse.Data.Repositories;
using Warehouse.Model.Models;
using Warehouse.Service.InterfacesServices;
using Warehouse.Service.Services;

namespace Warehouse.Service
{
    public class ProductService : Service<Product>, IProductService
    {
        private IProductRepository ProductReposiotry;

        public ProductService(IProductRepository productReposiotry)
        {
            ProductReposiotry = productReposiotry;
        }
    }
}
