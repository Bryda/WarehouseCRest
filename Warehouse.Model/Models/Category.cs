using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Warehouse.Model.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Field is required!")]
        public string Name { get; set; }
        [JsonIgnore]
        public virtual ICollection<Product> Products { get; set; }
    }
}